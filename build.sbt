ThisBuild / version := "0.1.-MVP"

ThisBuild / scalaVersion := "2.13.14"

// https://mvnrepository.com/artifact/org.apache.kafka/kafka-clients
libraryDependencies += "org.apache.kafka" % "kafka-clients" % "3.7.0"
lazy val root = (project in file("."))
  .settings(
    name := "ScalaUrlSourcer"
  )
