import org.apache.kafka.clients.admin.AdminClient
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}

import scala.io.Source.fromURL
import java.util.Properties

object UrlSourcer {
  def main(args: Array[String]): Unit = {

    val ISS_TELEMETRY_URL = "https://api.wheretheiss.at/v1/satellites/25544"
    val CSS_TELEMETRY_API_KEY = "&apiKey=MYB6YL-ZLEDH7-LDPAUZ-598F"
    val CSS_NORAD_CODE = "48274"
    val N2YO_API = "https://api.n2yo.com/rest/v1/satellite/tle/"
    val CSS_NORAD_URL = N2YO_API + CSS_NORAD_CODE + CSS_TELEMETRY_API_KEY

    val  props = new Properties()
    props.put("bootstrap.servers", "localhost:29092")
    props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    props.put("request.timeout.ms","500")

    val producer = new KafkaProducer[String, String](props)
    val TOPIC = "spaceships"
    try {
      while (true) {
        val issJson = fromURL(ISS_TELEMETRY_URL).mkString
        println(issJson)
        val cssJson = fromURL(CSS_NORAD_URL).mkString
        println(cssJson)
        val recordISS = new ProducerRecord(TOPIC, "iss", issJson)
        producer.send(recordISS)
        val recordCSS = new ProducerRecord(TOPIC,"css", cssJson)
        producer.send(recordCSS)
        producer.flush()
        Thread.sleep(4000)
      }
    }
    finally {
      producer.close()
    }
  }
}
